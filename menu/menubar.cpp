#include "menubar.h"
#ifndef _MENUBAR_
#define _MENUBAR_

int min(int a, int b)
{
    return a <= b ? a : b;
}

MenuBar::MenuBar(string title) : currentMenu(nullptr),
                                 root(MenuItem(title))
{
    currentMenu = &root;
    this->is_inited = false;
}

void MenuBar::addItem(MenuItem *item)
{
    root.addItem(item);
    // update();
}

void MenuBar::enter()
{
    MenuItem *selected = currentMenu->getItems()[cursorPos];
    if (selected->getItems().size() > 0)
    {
        currentMenu = selected;
        cursorPos = 0;
        windowPos = 0;
        // update();
    }
    else {
        selected->select();
    }
}

void MenuBar::up()
{
    if (cursorPos > 0) cursorPos--;
    else cursorPos = currentMenu->getItems().size() - 1;

    if (cursorPos < windowPos) windowPos = cursorPos;
    if (cursorPos > windowPos + 3) windowPos = cursorPos - 3;

    // update();
}

void MenuBar::down()
{
    if (cursorPos < currentMenu->getItems().size() - 1) cursorPos++;
    else cursorPos = 0;

    if (cursorPos < windowPos) windowPos = cursorPos;
    if (cursorPos > windowPos + 3) windowPos = cursorPos - 3;

    // update();
}

void MenuBar::back()
{
    if (currentMenu->parent != nullptr)
    {
        currentMenu = currentMenu->parent;
        cursorPos = 0;
        windowPos = 0;
        // update();
    }
}

void MenuBar::update()
{
    LCD_inner->LCDclear();

    // draw title
    LCD_inner->LCDsetTextColor(WHITE);
    LCD_inner->LCDfillrect(0, 0, 84, 11, BLACK);
    LCD_inner->LCDdrawstring_P((84 - currentMenu->getTitle().size() * 6) / 2, 2, currentMenu->getTitle().c_str());

    // draw background cursor
    LCD_inner->LCDfillrect(2, ITEM_START_POS + ITEM_SIZE * (cursorPos - windowPos), 80, 9, BLACK);

    // draw item

    for (int i = windowPos; i < min(windowPos + 2, currentMenu->getItems().size()); i++) {

        string text = currentMenu->getItems()[i]->getTitle();
        int amount  =   currentMenu->getItems()[i]->order_amount;
        if (text.size() > 13) text = text.substr(0, 13);
        LCD_inner->LCDsetTextColor(BLACK);
        if (i == cursorPos) {
            LCD_inner->LCDsetTextColor(WHITE);
            LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos) + 1, text.c_str());
        }
        else if (i > cursorPos) {
            LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos) + 2, text.c_str());
        }
        else {
            LCD_inner->LCDdrawstring_P(3, ITEM_START_POS + ITEM_SIZE * (i - windowPos), text.c_str());
        }
    }


    LCD_inner->LCDdrawrect(0, 0, 84, 48, BLACK);
    LCD_inner->LCDsetTextColor(BLACK);
    delay(100);
    LCD_inner->LCDdisplay();
}

int MenuBar::showDialog(string title, string message, int flag, int duration)
{
    if (flag == POWER_DIALOG) {
        dialog.clearButton();
        dialog.addButton(new Button("", 0, 0, SHUTDOWN, SHUTDOWN_HOVER));
        dialog.addButton(new Button("", 0, 0, RESTART, RESTART_HOVER));
        dialog.addButton(new Button("", 0, 0, CANCEL, CANCEL_HOVER));
    }
    else if (flag == YESNO_DIALOG) {
        dialog.clearButton();
        dialog.addButton(new Button("", 0, 0, ACCEPT, ACCEPT_HOVER));
        dialog.addButton(new Button("", 0, 0, CANCEL, CANCEL_HOVER));
    }
    else if (flag == OPENFILE_DIALOG) {
        dialog.clearButton();
        dialog.addButton(new Button("", 0, 0, OPEN, OPEN_HOVER));
        dialog.addButton(new Button("", 0, 0, RUN, RUN_HOVER));
    }
    else if (flag == ACCEPT_DIALOG) {
        dialog.clearButton();
        dialog.addButton(new Button("", 0, 0, ACCEPT, ACCEPT_HOVER));
    }
    
    int status = dialog.show(title, message, duration);
    if (flag != MESSAGE_DIALOG) update();
    return status;
}
#endif