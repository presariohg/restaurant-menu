#include "button.h"

Button::Button(string text, int x, int y, int image, int hover_image)
{
    this->text = text;
    this->image = image;
    this->hover_image = hover_image;
    this->x = x;
    this->y = y;
}

void Button::setPosition(int x, int y)
{
    this->x = x;
    this->y = y;
}

int Button::getImage()
{
    return image;
}

void Button::draw(bool hover)
{
    LCD_inner->LCDsetFont(font5x5);

    if (image == NO_IMAGE)
    {
        if (hover)
        {
            LCD_inner->LCDfillrect(x, y - 1, 5 * text.size()+ 1, 7, BLACK);
            LCD_inner->LCDsetTextColor(WHITE);
            LCD_inner->LCDdrawstring_P(x, y, text.c_str());
        }
        else
        {
            LCD_inner->LCDsetTextColor(BLACK);
            LCD_inner->LCDdrawstring_P(x, y, text.c_str());
        }
    }
    else
    {
        if (hover)
        {
            LCD_inner->LCDdrawbitmap(x, y, IMG[hover_image], IMG_SIZE[hover_image].x, IMG_SIZE[hover_image].y, BLACK);
        }
        else
        {
            LCD_inner->LCDdrawbitmap(x, y, IMG[image], IMG_SIZE[image].x, IMG_SIZE[image].y, BLACK);
        }
    }
}

void Button::setAction(function<void ()> cb)
{
    callback = cb;
}

void Button::click()
{
    callback();
}
