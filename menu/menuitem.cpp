#include "menuitem.h"

MenuItem::MenuItem(string title, int price, int index) : parent(nullptr)
{
    this->title = title;
    this->price = price;
    this->index = index;
}

void MenuItem::addItem(MenuItem *item)
{
    items.push_back(item);
    item->parent = this;
}

vector<MenuItem *> MenuItem::getItems() const
{
    return items;
}

string MenuItem::getTitle()
{
    return title;
}

void MenuItem::setTitle(string title)
{
    this->title = title;
}

void MenuItem::select()
{
    callback();
}

void MenuItem::setAction(function<void()> cb)
{
    callback = cb;
}
