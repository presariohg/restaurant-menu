/****************************************************************************
Font created by the LCD Vision V1.05 font & image editor/converter
(C) Copyright 2011-2013 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Big_number_digital
Fixed font width: 10 pixels
Font height: 14 pixels
First character: 0x20
Last character: 0x7F

Exported font data size:
2692 bytes for displays organized as horizontal rows of bytes
1924 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

const unsigned char big_number_digital[]=
{
0x0A, /* Fixed font width */
0x0E, /* Font height */
0x30, /* First character */
0x0A, /* Number of characters in font */
	
/* Font data for displays organized as
   rows of vertical bytes */

/* Code: 0x20, ASCII Character: '0' */
0xC0, 0x78, 0x0F, 0x03, 0x01, 0x01, 0x01, 0x03, 
0x06, 0xFC, 0x07, 0x0C, 0x18, 0x10, 0x30, 0x20, 
0x30, 0x18, 0x0E, 0x03, 

/* Code: 0x31, ASCII Character: '1' */
0x20, 0x30, 0x18, 0x0C, 0xFE, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x07, 0x00, 
0x00, 0x00, 0x00, 0x00, 

/* Code: 0x32, ASCII Character: '2' */
0x00, 0x01, 0x86, 0xE2, 0x3E, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x0C, 0x0F, 0x08, 0x08, 0x08, 
0x08, 0x08, 0x08, 0x08, 

/* Code: 0x33, ASCII Character: '3' */
0x00, 0x00, 0x00, 0x22, 0x32, 0x3E, 0x4C, 0xC0, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x04, 
0x06, 0x03, 0x00, 0x00, 

/* Code: 0x34, ASCII Character: '4' */
0x80, 0xE0, 0x38, 0x0E, 0xF8, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x03, 0x02, 0x02, 0x02, 0x1F, 0x03, 
0x01, 0x01, 0x01, 0x00, 

/* Code: 0x35, ASCII Character: '5' */
0x00, 0x00, 0xF8, 0xCC, 0x44, 0xC4, 0x84, 0x84, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x08, 0x08, 
0x0C, 0x07, 0x00, 0x00, 

/* Code: 0x36, ASCII Character: '6' */
0x00, 0x00, 0x00, 0xF0, 0xD8, 0x44, 0xC2, 0x80, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x0C, 0x08, 
0x08, 0x0F, 0x03, 0x00, 

/* Code: 0x37, ASCII Character: '7' */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 

/* Code: 0x38, ASCII Character: '8' */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 

/* Code: 0x39, ASCII Character: '9' */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 
};

